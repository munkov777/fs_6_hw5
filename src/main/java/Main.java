import Enums.Species;

import java.time.DayOfWeek;

public class Main {
    public static void main(String[] args) {
        Human viktor = new Human("Viktor", "Munko",1982);
        viktor.setIq(150);
        String[][] scheduleViktor = new String[2][2];
        scheduleViktor[0][0] = DayOfWeek.SATURDAY.name();
        scheduleViktor[0][1] = "Play football";
        scheduleViktor[1][0] = DayOfWeek.SUNDAY.name();
        scheduleViktor[1][1] = "Goto the gym";
        viktor.setSchedule(scheduleViktor);
        Human halyna = new Human("Halyna","Munko",1988);
        halyna.setIq(150);
        String[][] scheduleHalyna = new String[2][2];
        scheduleHalyna[0][0] = DayOfWeek.SATURDAY.name();
        scheduleHalyna[0][1] = "Go shopping";
        scheduleHalyna[1][0] = DayOfWeek.SUNDAY.name();
        scheduleHalyna[1][1] = "Play tennis";
        halyna.setSchedule(scheduleHalyna);
        Family familyWick = new Family(viktor, halyna);
        Pet dog = new Pet(Species.DOG, "Rej", 60,new String[]{"play"});
        familyWick.setPet(dog);
        Human tymur =new Human("Tymur", "Munko", 12);
        familyWick.addChild(tymur);   //
        tymur.setFamily(familyWick);  //







    }
}
