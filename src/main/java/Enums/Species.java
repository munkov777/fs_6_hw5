package Enums;

public enum Species {
    CAT,
    DOG,
    FISH,
    PARROT,
    HAMSTER;

}
