import Enums.Species;

import java.util.Arrays;

public class Pet {
    private Species species;
    private static String nickname;
    private static int age;
    private static int trickLevel;
    private String[] habits;

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(Species species, String nickname, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(){

    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public static String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public static int getAge() {
        return age;
    }

    public static void setAge(int age) {
        Pet.age = age;
    }

    public static int getTricklavel() {
        return trickLevel;
    }

    public void setTricklavel(int tricklavel) {
        if (trickLevel >= 0 & trickLevel <= 100) {
            this.trickLevel = trickLevel;
        } else {
            System.out.println("incorrect level of trick");
        }

    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public static void eat(){
        System.out.println("Я ї'м!");
    }

    public void respond(){
        System.out.printf("Привіт, хазяїн. Я - $s\t. Я скучив!", nickname);
    }
    public static void foul(){
        System.out.println("Потрібно добре замести сліди...");
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species='" + species + '\'' +
                ", nickname='" + nickname + '\'' +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }
}
