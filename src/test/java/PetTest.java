import Enums.Species;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;


public class PetTest {


    Pet pet = new Pet(Species.DOG,"Rej");
    Pet pet2 = new Pet(Species.CAT,"Lulu");

    @Test
    void testHashcode(){
        assertTrue(pet.hashCode()!=pet2.hashCode());
    }




    @Test
    void getNickname() {
        String expected = String.valueOf(pet.getNickname());
        String actual = pet.getNickname();
        Assert.assertEquals(expected, actual);

    }
    @Test
    void testEqualsFalse(){
        assertFalse(pet2.equals(pet));
    }


}
